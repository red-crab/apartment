﻿using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    public bool enableControlSight = true;
    public bool enableControlMovement = true;

    public float viewSensitivity = 1f;
    private float xRotation = 0.0f;
    private float yRotation = 0.0f;
    public float movementSpeed = 1f;

    public float raycastDist = 3f;
    private InteractableObject _previousDetect;

    private CharacterController _cc;
    private Camera _cam;

    void Awake()
    {
        if (_cc == null) {
            _cc = GetComponent<CharacterController>();
        }
        if (_cam == null) {
            _cam = Camera.main;
        }
        gameObject.tag = "Player";
    }

    void Update()
    {
        ViewControl();
        MoveControl();
        ScreenRaycast();
    }

    void ViewControl()
    {
        if (!enableControlSight) return;

        yRotation += Input.GetAxis("Mouse X") * viewSensitivity;
        xRotation -= Input.GetAxis("Mouse Y") * viewSensitivity;
        xRotation = Mathf.Clamp(xRotation, -90, 90);
        _cam.transform.eulerAngles = new Vector3(xRotation, yRotation, 0.0f);
    }

    void MoveControl()
    {
        if (!enableControlMovement) return;

        float horizontal = Input.GetAxis("Horizontal") * movementSpeed;
        float vertical = Input.GetAxis("Vertical") * movementSpeed;
        Vector3 target = _cam.transform.right * horizontal + _cam.transform.forward * vertical;
        target.y = transform.position.y;
        _cc.Move(target * Time.deltaTime);
    }

    void ScreenRaycast()
    {
        if (!enableControlSight) {
            if(_previousDetect != null) {
                _previousDetect.OnExitCursor();
                _previousDetect = null;
            }
            return;
        }

        RaycastHit hit;
        Ray ray = _cam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0.0f));
        if (Physics.Raycast(ray.origin, _cam.transform.forward, out hit, raycastDist)) {
            if(hit.collider.gameObject.CompareTag("Interactable")) {
                if(_previousDetect != null && _previousDetect.gameObject == hit.collider.gameObject) {
                    if (Input.GetButtonDown("Interact")) _previousDetect.OnActivate.Invoke();
                } else {
                    if(_previousDetect != null) _previousDetect.OnExitCursor();
                    _previousDetect = hit.collider.gameObject.GetComponent<InteractableObject>();
                    _previousDetect.OnEnterCursor();
                }
                // 위의 코드와 동일하게 동작하는 코드
                // 위의 코드가 불편하다면 아래의 코드를 참고하여 수정 요망
                //if (_previousDetect == null) {
                //    _previousDetect = hit.collider.gameObject.GetComponent<InteractableObject>();
                //    _previousDetect.OnEnterCursor();
                //} else if(_previousDetect.gameObject == hit.collider.gameObject) {
                //    if (Input.GetButtonDown("Interact")) _previousDetect.OnActivate.Invoke();
                //} else {
                //    _previousDetect.OnExitCursor();
                //    _previousDetect = hit.collider.gameObject.GetComponent<InteractableObject>();
                //    _previousDetect.OnEnterCursor();
                //}
            } else if (_previousDetect != null) {
                _previousDetect.OnExitCursor();
                _previousDetect = null;
            }
        }
        else {
            if(_previousDetect != null) {
                _previousDetect.OnExitCursor();
                _previousDetect = null;
            }
        }
    }

    public void DisableCharacterControl()
    {
        enableControlMovement = false;
        enableControlSight = false;
    }

    public void EnableCharacterControl()
    {
        enableControlMovement = true;
        enableControlSight = true;
    }
}