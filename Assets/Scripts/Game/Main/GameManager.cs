﻿using System;
using System.Collections;
using System.Collections.Generic;

public class GameManager : Singleton<GameManager>
{
    GameManager()
    {

    }

    private static ProgressSystem _progress;
    private static LockSystem _lock;
    
    public static ProgressSystem Progress
    {
        get
        {
            if (_progress == null)
                _progress = new ProgressSystem();
            return _progress;
        }
    }
    public static LockSystem Lock
    {
        get
        {
            if (_lock == null)
                _lock = new LockSystem();
            return _lock;
        }
    }
}