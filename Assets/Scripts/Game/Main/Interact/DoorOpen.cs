﻿using System.Collections;
using UnityEngine;

// 사용하기 전, 문의 경첩 역할을 하는 임의의 GameObject를 생성해주어야 함
public class DoorOpen : MonoBehaviour
{
    public float openingSpeed;
    public float doorOpeningAngle;
    public float handleClickAngle;

    // 두 손잡이 사이에 임의의 GameObject를 생성하여 손잡이의 회전축 역할을 해야 함
    public Transform handle;

    public InteractableObject[] interactableObjects;
    public string openedStr;
    public string closedStr;
    public string lockedStr;

    private float _doorAccum;
    private float _handleAccum;
    private bool _isLocked;
    private bool _isRunning;
    private bool _isOpened;
    private Vector3 _doorDir;

    void Awake()
    {
        if (openingSpeed < 0.1f) openingSpeed = 100.0f;
        if (Mathf.Abs(doorOpeningAngle) < 0.1f) doorOpeningAngle = 90.0f;
        if (handleClickAngle < 0.1f) handleClickAngle = 30.0f;
        _doorAccum = 0.0f;
        _handleAccum = 0.0f;
        LockDoor(false);
        _isRunning = false;
        _isOpened = false;
        if (doorOpeningAngle > 0.0f) _doorDir = Vector3.up;
        else _doorDir = Vector3.down;
    }

    public void OpenOrClose()
    {
        if (_isOpened) CloseDoor(false);
        else OpenDoor();
    }

    public void OpenDoor()
    {
        if (_isLocked || _isRunning || _isOpened) return;
        _isRunning = true;
        StartCoroutine(Open());
        if (handle != null) StartCoroutine(Handle());
    }

    public void CloseDoor(bool lockAfterClose)
    {
        if (_isLocked || _isRunning || (_isOpened == false)) return;
        _isRunning = true;
        StartCoroutine(Close(lockAfterClose));
        if (handle != null) StartCoroutine(Handle());
    }

    public void LockDoor(bool val)
    {
        _isLocked = val;
        if (interactableObjects != null) {
            for (int i = 0; i < interactableObjects.Length; i++) {
                if(_isLocked) interactableObjects[i].displayName = lockedStr;
                else {
                    if (_isOpened) interactableObjects[i].displayName = openedStr;
                    else interactableObjects[i].displayName = closedStr;
                }
            }
        }
    }

    IEnumerator Open()
    {
        if (interactableObjects != null) {
            for (int i = 0; i < interactableObjects.Length; i++) {
                interactableObjects[i].displayName = "";
            }
        }
        while (_doorAccum < Mathf.Abs(doorOpeningAngle)) {
            float val = openingSpeed * Time.deltaTime;
            if (_doorAccum + val > Mathf.Abs(doorOpeningAngle)) {
                val = Mathf.Abs(doorOpeningAngle) - _doorAccum;
                transform.Rotate(_doorDir * val);
            } else {
                transform.Rotate(_doorDir * val);
            }
            _doorAccum += val;
            yield return null;
        }
        _isRunning = false;
        _isOpened = true;
        if (interactableObjects != null) {
            for (int i = 0; i < interactableObjects.Length; i++) {
                interactableObjects[i].displayName = openedStr;
            }
        }
    }

    IEnumerator Close(bool lockAfterClose)
    {
        if (interactableObjects != null) {
            for (int i = 0; i < interactableObjects.Length; i++) {
                interactableObjects[i].displayName = "";
            }
        }
        while (_doorAccum > 0.0f) {
            float val = openingSpeed * Time.deltaTime;
            if (_doorAccum - val < 0.0f) {
                val = _doorAccum;
                transform.Rotate(_doorDir * -1 * val);
            } else {
                transform.Rotate(_doorDir * -1 * val);
            }
            _doorAccum -= val;
            yield return null;
        }
        _isRunning = false;
        _isOpened = false;
        LockDoor(lockAfterClose);
    }

    IEnumerator Handle()
    {
        while(_handleAccum < handleClickAngle) {
            float val = openingSpeed * Time.deltaTime;
            _handleAccum += val;
            if (_handleAccum > handleClickAngle) {
                val -= _handleAccum - handleClickAngle;
                handle.Rotate(Vector3.right * val);
            } else {
                handle.Rotate(Vector3.right * val);
            }
            yield return null;
        }
        while(_handleAccum > 0.0f) {
            while (_handleAccum > 0.0f) {
                float val = openingSpeed * Time.deltaTime;
                _handleAccum -= val;
                if (_handleAccum < 0.0f) {
                    val -= 0.0f - _handleAccum;
                    handle.Rotate(Vector3.left * val);
                } else {
                    handle.Rotate(Vector3.left * val);
                }
                yield return null;
            }
        }
        yield return null;
    }
    
}
