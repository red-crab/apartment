﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawerOpen : MonoBehaviour
{
    public float openingSpeed;
    public float openingRange;
    public Vector3 targetDir;

    private float _accum;
    private bool _isRunning;
    private bool _isOpened;

    void Awake()
    {
        if (openingSpeed < 0.01f) openingSpeed = 0.5f;
        if (openingRange < 0.01f) openingRange = 0.3f;
        if (targetDir == Vector3.zero) targetDir = Vector3.forward;
        _accum = 0.0f;
        _isRunning = false;
        _isOpened = false;
    }

    public void OpenOrClose()
    {
        if (_isOpened) CloseDrawer();
        else OpenDrawer();
    }

    public void OpenDrawer()
    {
        if (_isRunning || _isOpened) return;
        _isRunning = true;
        StartCoroutine(Open());
    }

    public void CloseDrawer()
    {
        if (_isRunning || (_isOpened == false)) return;
        _isRunning = true;
        StartCoroutine(Close());
    }

    IEnumerator Open()
    {
        while (_accum < openingRange) {
            float val = openingSpeed * Time.deltaTime;
            if (_accum + val > openingRange) {
                val = openingRange - _accum;
                transform.Translate(targetDir * val);
            } else {
                transform.Translate(targetDir * val);
            }
            _accum += val;
            yield return null;
        }
        _isRunning = false;
        _isOpened = true;
    }

    IEnumerator Close()
    {
        while (_accum > 0.0f) {
            float val = openingSpeed * Time.deltaTime;
            if (_accum - val < 0.0f) {
                val = _accum;
                transform.Translate(targetDir * -1 * val);
            } else {
                transform.Translate(targetDir * -1 * val);
            }
            _accum -= val;
            yield return null;
        }
        _isRunning = false;
        _isOpened = false;
    }
}
