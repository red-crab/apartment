﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class InteractableObject : MonoBehaviour
{
    public string displayName {
        get { return guideMessage.text; }
        set { guideMessage.text = value; }
    }
    public Text guideMessage;
    public UnityEvent OnActivate;

    void Awake()
    {
        if(gameObject.GetComponent<MeshCollider>() == null) {
            gameObject.AddComponent<MeshCollider>();
        }
        gameObject.tag = "Interactable";
        

        if(guideMessage == null) {
            var obj = GameObject.Find("Guide message");
            if(obj == null) {
                Debug.Log("No Guide message");
            }
            guideMessage = obj.GetComponent<Text>();
        }
    }

    public void OnEnterCursor()
    {
        guideMessage.gameObject.SetActive(true);
    }

    public void OnExitCursor()
    {
        guideMessage.gameObject.SetActive(false);
    }
}
