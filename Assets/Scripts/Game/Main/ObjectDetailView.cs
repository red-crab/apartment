﻿using System.Collections;
using UnityEngine;

public class ObjectDetailView : MonoBehaviour
{
    public float transTime;
    public float rotationSensivity;

    private Camera _cam;
    private GameObject _selected;
    private PlayerControl _player;

    void Awake()
    {
        if (transTime < 0.01f) transTime = 0.5f;
        if (rotationSensivity < 0.01f) rotationSensivity = 100.0f;
        if (_cam == null) _cam = Camera.main;
        if (_player == null) _player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerControl>();
    }

    public void OnSelectObject(GameObject obj)
    {
        _selected = obj;
        _player.DisableCharacterControl();
        StartCoroutine(DetailView());
    }

    IEnumerator DetailView()
    {
        Vector3 targetPos = _cam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0.0f)) + _cam.transform.forward;

        Vector3 originPos = _selected.transform.localPosition;
        Quaternion originRot = _selected.transform.localRotation;

        float accum = 0.0f;
        while (!Input.GetButtonDown("Cancel")) {
            if(accum < transTime) {
                accum += Time.deltaTime;
                _selected.transform.localPosition = Vector3.Lerp(originPos, targetPos, accum / transTime);
                _selected.transform.localRotation = Quaternion.Lerp(originRot, _cam.transform.localRotation, accum / transTime);
            }
            else if (Input.GetButton("Fire1")) {
                _selected.transform.Rotate((Input.GetAxis("Mouse Y") * rotationSensivity * Time.deltaTime),
                    (Input.GetAxis("Mouse X") * -1 * rotationSensivity * Time.deltaTime), 0.0f, Space.World);
            }
            yield return null;
        }

        Quaternion prevRot = _selected.transform.localRotation;
        accum = 0.0f;
        while (accum < transTime) {
            accum += Time.deltaTime;
            _selected.transform.localPosition = Vector3.Lerp(targetPos, originPos, accum / transTime);
            _selected.transform.localRotation = Quaternion.Lerp(prevRot, originRot, accum / transTime);
            yield return null;
        }

        _player.EnableCharacterControl();
        yield return null;
    }
}
