﻿using UnityEngine;
using UnityEngine.Events;

public class InvisibleTrigger : MonoBehaviour
{
    private Renderer _renderer;
    public UnityEvent enterEvents;
    public UnityEvent stayEvents;
    public UnityEvent ExitEvents;

    void Awake()
    {
        if(_renderer == null) _renderer = GetComponent<Renderer>();
        _renderer.enabled = false;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player") enterEvents.Invoke();
    }

    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player") stayEvents.Invoke();
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player") ExitEvents.Invoke();
    }
}
