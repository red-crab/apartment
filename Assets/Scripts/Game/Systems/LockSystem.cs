﻿using System.Collections.Generic;
using UnityEngine;

// 게임의 진행에 있어 사전에 요구되는 행동이 무엇인지 담고 있는 클래스.
// 실제 게임의 진행상황은 담고있지 않으니 진행상황을 알고 싶으면 LockSystem 이용
public class LockSystem
{
    List<ProgressTransition> transitionList;
    public List<ProgressTransition> TransitionList
    {
        get
        {
            if (transitionList == null)
                transitionList = new List<ProgressTransition>();
            return transitionList;
        }
    }

    public LockSystem()
    {
        transitionList = new List<ProgressTransition>();
    }
    
    public bool AddTransition(ProgressFlag condition, params ProgressFlag[] required)
    {
        if (Exist(condition))
            return false;

        var p = new ProgressTransition(condition, required);
        transitionList.Add(p);
        return true;
    }

    private bool Exist(ProgressFlag cond)
    {
        foreach (var req in transitionList)
        {
            if (req.condition == cond)
                return true;
        }
        return false;
    }
}

public class ProgressTransition
{
    public ProgressFlag condition;
    public List<ProgressFlag> pre_required;

    public ProgressTransition()
    {
        condition = ProgressFlag.None;
        pre_required = new List<ProgressFlag>();
    }

    public ProgressTransition(ProgressFlag cond, params ProgressFlag[] pre_req)
    {
        condition = cond;
        pre_required = new List<ProgressFlag>();
        pre_required.AddRange(pre_req);
    }
}