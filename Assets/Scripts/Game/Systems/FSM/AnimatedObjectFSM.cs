﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatedObjectFSM : BasicFSM
{
    public void PlayAnimation()
    {
        SetState(FSMAnimatedObjectState.PlayAnim);
    }

    IEnumerator PlayAnim()
    {
        _anim.SetInteger("state", FSMAnimatedObjectState.PlayAnim.Value);
        do {
            yield return null;
            if (_stateChanged) break;
        } while (state == FSMAnimatedObjectState.PlayAnim);
    }
}

public class FSMAnimatedObjectState : FSMBasicState
{
    public static readonly FSMAnimatedObjectState PlayAnim = new FSMAnimatedObjectState("PlayAnim", 1);

    private FSMAnimatedObjectState(string str, int n) : base(str, n)
    {
    }
}
