﻿using System.Collections;
using UnityEngine;

public class BasicFSM : MonoBehaviour
{
    protected FSMBasicState state;
    protected bool _stateChanged;

    public Animator _anim;

    void Awake()
    {
        _anim = GetComponent<Animator>();
        if(state == null) state = FSMBasicState.Default;
    }

    void OnEnable()
    {
        StartCoroutine(FSMMain());
    }

    public void SetState(FSMBasicState s)
    {
        _stateChanged = true;
        state = s;
    }

    IEnumerator FSMMain()
    {
        while (true) {
            _stateChanged = false;
            yield return StartCoroutine(state.ToString());
        }
    }

    IEnumerator Default()
    {
        // 상태 진입 시 코드
        // animator의 state 변경은 SetState() 시에 하게 되면 이처럼 모든 함수에 하나하나 SetInteger를 써주지 않아도 되지만
        // Coroutine의 상태 종료 시 코드보다 먼저 실행되기 때문에 여기에 넣는다.
        // 추후 설계를 변경하게 되면 위 내용을 고려해야함
        _anim.SetInteger("state", FSMBasicState.Default.Value);

        do {
            yield return null;
            if (_stateChanged) break;
            // 상태 동작 코드

        } while (state == FSMBasicState.Default);

        // 상태 종료 시 코드
    }
}

public class FSMBasicState
{
    public static readonly FSMBasicState Default = new FSMBasicState("Default", 0);

    public int Value { get; set; }
    public string Name { get; set; }

    protected FSMBasicState(string name, int value)
    {
        this.Name = name;
        this.Value = value;
    }

    public override string ToString()
    {
        return Name;
    }
}