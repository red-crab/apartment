﻿using System.Collections;

public class CreatureFSM : BasicFSM
{
    public void Appear(int state)
    {
        this.state = FSMCreatureState.ToState(state);
        gameObject.SetActive(true);
    }

    IEnumerator EatSilhouette()
    {
        _anim.SetInteger("state", FSMCreatureState.EatSilhouette.Value);
        do
        {
            yield return null;
            if (_stateChanged) break;
        } while (state == FSMCreatureState.EatSilhouette);
    }

    public void Disappear()
    {
        gameObject.SetActive(false);
    }
}

public class FSMCreatureState : FSMBasicState
{
    public static readonly FSMCreatureState Idle            = new FSMCreatureState("Idle", 1);
    public static readonly FSMCreatureState Walk            = new FSMCreatureState("Walk", 2);
    public static readonly FSMCreatureState SitAndEat       = new FSMCreatureState("SitAndEat", 5);
    public static readonly FSMCreatureState LookAround      = new FSMCreatureState("LookAround", 6);
    public static readonly FSMCreatureState EatSilhouette   = new FSMCreatureState("EatSilhouette", 11);
    public static readonly FSMCreatureState HandSilhouette  = new FSMCreatureState("HandSilhouette", 21);
    public static readonly FSMCreatureState GameOver        = new FSMCreatureState("GameOver", 99);



    private FSMCreatureState(string str, int n) : base(str, n)
    {
    }

    public static FSMCreatureState ToState(int val)
    {
        if (val == Idle.Value) return Idle;
        else if (val == Walk.Value) return Walk;
        else if (val == SitAndEat.Value) return SitAndEat;
        else if (val == LookAround.Value) return LookAround;
        else if (val == EatSilhouette.Value) return EatSilhouette;
        else if (val == HandSilhouette.Value) return HandSilhouette;
        else if (val == GameOver.Value) return GameOver;
        else return Idle;
    }
}
