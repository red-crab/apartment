﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Inactive 상태의 Gameobject는 Gameobject.Find 함수로 찾을 수 없어서 만든 클래스
// 유니티 구조상 Awake() -> Start() -> Update() 순서로 실행되기 때문에
// 꼼수로 Start 시에 Object들을 Inactive 상태로 바꾼다.
public class ObjectHider : MonoBehaviour
{
    public GameObject[] objects;
    void Start()
    {
        for(int i = 0; i < objects.Length; ++i) {
            objects[i].SetActive(false);
        }
    }
}
