﻿
// 게임의 진행 정보를 담고 있는 클래스
// flag가 존재하면 플레이어가 플레이 한 상태(Unlocked)
public class ProgressSystem
{
    private ProgressFlag _flag;

    public ProgressSystem()
    {
        UnCompleteAll();
    }

    public void UnComplete(params ProgressFlag[] flags)
    {
        for (int i = 0; i < flags.Length; i++)
        {
            _flag &= ~flags[i];
        }
    }
    
    public void Complete(params ProgressFlag[] flags)
    {
        for (int i = 0; i < flags.Length; i++)
        {
            _flag |= flags[i];
        }
    }

    public bool IsCompleted(params ProgressFlag[] flags)
    {
        for (int i = 0; i < flags.Length; i++)
        {
            if (_flag.HasFlag(flags[i]))
                return true;
        }
        return false;
    }

    public void UnCompleteAll()
    {
        _flag = ProgressFlag.None;
    }

    public void CompleteAll()
    {
        _flag = ProgressFlag.All;
    }
    
    public ProgressState GetProgressState(ProgressFlag flag)
    {
        ProgressState state;

        if (IsCompleted(flag))
        {
            state = ProgressState.Completed;
        }
        else
        {
            ProgressTransition req = null;
            for (int i = 0; i < GameManager.Lock.TransitionList.Count; i++)
            {
                if (GameManager.Lock.TransitionList[i].condition == flag)
                {
                    req = GameManager.Lock.TransitionList[i];
                    break;
                }
            }
            if (req == null || IsCompleted(req.pre_required.ToArray()))
            {
                state = ProgressState.Playable;
            }
            else
            {
                state = ProgressState.Locked;
            }
        }

        return state;
    }
}

public enum ProgressState
{
    Locked,
    Playable,
    Completed
}