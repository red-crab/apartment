﻿public enum ProgressFlag
{
    None = 1 << 0,

    // 1층
    Enter_first_floor = 1 << 1,
    Interact_with_guard = 1 << 2,
    Move_package = 1 << 3,
    Complete_first_floor = 1 << 99,

    // 4층
    Enter_fourth_floor = 1 << 100,
    Complete_fourth_floor = 1 << 199,

    // 5층
    Enter_fifth_floor = 1 << 200,
    Complete_fifth_floor = 1 << 299,

    // 6층
    Enter_sixth_floor = 1 << 300,
    Complete_sixth_floor = 1 << 399,



    // 도구
    Get_tool = 1 << 1000,
    Get_flashlight = 1 << 1001,
    Get_compass = 1 << 1002,


    // 수집품
    Get_collects = 1 << 2000,

    // 열쇠
    Get_key = 1 << 2010,
    Get_large_room_key = 1 << 2011,
    Get_frig_key = 1 << 2012,
    Get_508_room_key = 1 << 2013,
    Get_607_room_key = 1 << 2014,

    // 사진 조각
    Get_photo_piece = 1 << 2020,
    Get_photo_piece_01 = 1 << 2021,
    Get_photo_piece_02 = 1 << 2022,
    Get_photo_piece_03 = 1 << 2023,

    // 교리전
    Get_doctrine = 1 << 2030,
    Get_doctrine_01 = 1 << 2031,
    Get_doctrine_02 = 1 << 2032,
    Get_doctrine_03 = 1 << 2033,
    Get_doctrine_04 = 1 << 2034,

    // 제단
    Get_altar_piece = 1 << 2040,
    Get_altar_piece_01 = 1 << 2041,
    Get_altar_piece_02 = 1 << 2042,
    Get_altar_piece_03 = 1 << 2043,


    // 잡동사니
    Get_etc = 1 << 2100,
    Get_video_tape = 1 << 2101,
    Get_mouse_toy = 1 << 2102,
    Get_secret_door_handle = 1 << 2103,
    Get_doll = 1 << 2104,
    Get_cutter_knife = 1 << 2105,
    Get_charm = 1 << 2106,
    Get_landscape_painting = 1 << 2107,
    Get_black_candle = 1 << 2108,

    All = ~(1 << 999999),
}
