﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class LoadProgressTransition
{
    public static bool Load()
    {
        var text_asset = Resources.Load("ProgressTransition") as TextAsset;
        if (text_asset == null) { return false; }

        var txt = text_asset.text.Split('\n');
        List<ProgressFlag> req_list = new List<ProgressFlag>();
        for (int i = 0; i < txt.Length; i++)
        {
            var str = txt[i].Split(',');

            if (str[0] == "") { continue; }
            var c1 = Enum.TryParse(str[0], out ProgressFlag cond);
            if(!c1)
            {
                Debug.Log("Failed to load condition : " + str[0]);
                continue;
            }

            req_list.Clear();
            for (int j = 1; j < str.Length; ++j)
            {
                if (str[j] == "") { continue; }
                var c2 = Enum.TryParse(str[j], out ProgressFlag req);
                if (!c1)
                {
                    Debug.Log("Failed to load requirement : " + str[j]);
                    continue;
                }
                req_list.Add(req);
            }
            GameManager.Lock.AddTransition(cond, req_list.ToArray());
        }
        return true;
    }
}