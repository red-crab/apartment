using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T _instance ;

    public static T Instance
    {
        get
        {
            if (_instance == null)
            {
                var obj = GameObject.Find(typeof(T).Name);
                if (obj != null)
                {
                    _instance = obj.GetComponent<T>();
                }
                else
                {
                    var new_obj = new GameObject(typeof(T).Name);
                    _instance = new_obj.AddComponent<T>();
                }
            }
            return _instance;
        }
    }

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
    
}
