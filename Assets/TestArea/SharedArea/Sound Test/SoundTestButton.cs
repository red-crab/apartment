﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundTestButton : MonoBehaviour
{
    public AudioSource source;

    void Awake()
    {
        source = GetComponent<AudioSource>();
        source.Stop();
    }

    public void OnClickButton()
    {
        if(source.clip == null)
        {
            Debug.Log("No sound clip in " + gameObject.name);
            return;
        }

        source.Play();
        Debug.Log("Play " + source.clip.name);
    }
}
